## Базы данных и схемы.  
  
После инициализации кластера БД создается три базы данных: *postgres*, *template0* и *template1*. К *postgres* происходит подключение по умолчанию, к *template0* подключение запрещено. PG создает все базы путем копирования из шаблона.  
```
postgres$ psql -l
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges   
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
(3 rows)
```
```
=> SELECT datname, datistemplate, datallowconn, datconnlimit FROM pg_database;
  datname  | datistemplate | datallowconn | datconnlimit 
-----------+---------------+--------------+--------------
 postgres  | f             | t            |           -1
 template1 | t             | t            |           -1
 template0 | t             | f            |           -1
(3 rows)

 - datistemplate - является ли база данных шаблоном;
 - datallowconn - разрешены ли соединения с базой данных,
 - datconnlimit - максимальное количество соединений (-1 = без ограничений).
```
  
Схемы - представляют собой пространство имен для объектов БД. Их можно сравнить с директориями в ФС. Они позволяют разделить объекты на логические группы, для управления ними и предотвращения конфликтов. В каждой БД по умолчанию создаются схемы *pg_catalog* и *public*. В *public* попадают таблицы, которым явно не указана схема. В схеме *pg_catalog* лежат системные таблицы. В PG имеется параметр *search_path*, который работает как *$PATH* в Unix. Также есть параметр *current_schemas*, который отображет реальный путь поиска. Он может отличаться, потому что есть несуществующие схемы, схемы к которым у пользователя нет доступа и т.д.. Также существуют схемы по имени пользователя, они указаны в *search_path*, но по умолчанию не существуют, если создать такую схему, то все таблицы по умолчанию буду падать в неё, а не в *public*.  
  
В PG существуют временные таблицы, в которых хранится информация, которая должна быть доступна только текущему сеансу|транзакции. Временный таблицы не журналируемы, они не попадают в кэш. Временные таблицы организованы с помощью схем, для сеанса создается схема *pg_temp_N*. Обращаться к ней можно по алиасу *pg_temp* или явно. После окончания сеанса все объекты схемы удаляются, а сама схема остается для последующего использования.  
  
Размер БД можно узнать с помощью функции:  
```
=> SELECT pg_size_pretty(pg_database_size('appdb'));
 pg_size_pretty 
----------------
 7054 kB
(1 row)
```
Список схем можно узнать по команде `\\dn`:
```
=> \dn
  List of schemas
  Name  |  Owner   
--------+----------
 app    | postgres
 public | postgres
(2 rows)
```
Список таблиц можно узнать по команде `\\dn`:
```
=> \dt
        List of relations
 Schema | Name | Type  |  Owner   
--------+------+-------+----------
 public | t    | table | postgres
(1 row)
```  
  
## Системный каталог.  
  
Системный каталог - набор таблиц и представлений, описывающих все таблицы кластера баз данных. Все таблицы и представления системного каталога находятся в схеме *pg_catalog*. Для удобной работы с системным каталогом можно воспользоваться командами, начинающимися на `\\d` (describe). В кластере БД для каждой БД создается свой набор таблиц системного каталога. Но также существует несколько объектов каталога, которые являются общими для всего кластера. Эти таблицы хранятся вне какой-либо БД, но одинаково видны из любой БД. Список объектов:
 1. Базы даннных: pg_database;
 2. Табличные пространства: pg_tablespace;
 3. Роли: pg_authid, pg_auth_members;
 4. Настройки для ролей и БД: pg_db_role_settings;
 5. Подписки логической репликации: pg_subscription;
 6. Шаблоны процедурных языков: pg_pltemplate;
 7. Зависимости, описания и метки безопасности для общих объектов: pg_shdepend, pg_shdescription, pg_shseclabel.  
   
Все таблицы системного каталога начинаются с префикса *pg_*. Названия стоблцов имеют трехбуквенный префикс, который, как правилло, соответсвует имени таблицы. Таблицы системного каталога используют в качестве первичного ключа тип *OID*. Это целочисленный тип данных с разрядностью 32 бита и автоинкрементом. *OID* нельзя пользоваться в своих таблицах, можно пользоваться *serial*. OID - скрытый стобец, нужно указывать явно. Есть псевдонимы OID, начинающиеся на *reg*, которые позволяют преобразовывать OID и имя и обратно.  
Примеры некоторых таблиц системного каталога:  
Инфо о БД:  
```
=> SELECT * FROM pg_database WHERE datname = 'data_catalog' \gx
-[ RECORD 1 ]-+-------------
datname       | data_catalog
datdba        | 10
encoding      | 6
datcollate    | en_US.UTF-8
datctype      | en_US.UTF-8
datistemplate | f
datallowconn  | t
datconnlimit  | -1
datlastsysoid | 12327
datfrozenxid  | 548
datminmxid    | 1
dattablespace | 1663
datacl        | 
```
Инфо о схеме:  
```
=> SELECT * FROM pg_namespace WHERE nspname = 'public' \gx
-[ RECORD 1 ]---------------------------------
nspname  | public
nspowner | 10
nspacl   | {postgres=UC/postgres,=UC/postgres}
```  
Важная таблица pg_class хранит описание таблиц, представлений, индексов, последовательнойстей {отношение (relation)}, отсюда и префикс *rel*:  
```
=> SELECT relname, relkind, relnamespace, relfilenode, relowner, reltablespace 
FROM pg_class WHERE relname ~ '^(emp|top).*';
     relname      | relkind | relnamespace | relfilenode | relowner | reltablespace 
------------------+---------+--------------+-------------+----------+---------------
 employees_id_seq | S       |         2200 |       16454 |       10 |             0
 employees        | r       |         2200 |       16456 |       10 |             0
 employees_pkey   | i       |         2200 |       16463 |       10 |             0
 top_managers     | v       |         2200 |       16465 |       10 |             0
(4 rows)
```  
Типы объектов различаются по столбцу *relkind*.  
Для каждого типа объектов имеет смысл брать только часть столбцов. Для этого существуют различные представления:
```
=> SELECT schemaname, tablename, tableowner, tablespace
FROM pg_tables WhERE schemaname = 'public';
 schemaname | tablename | tableowner | tablespace 
------------+-----------+------------+------------
 public     | employees | postgres   | 
(1 row)

=> SELECT * 
FROM pg_views WHERE schemaname = 'public';
 schemaname |   viewname   | viewowner |              definition              
------------+--------------+-----------+--------------------------------------
 public     | top_managers | postgres  |  SELECT employees.id,               +
            |              |           |     employees.name,                 +
            |              |           |     employees.manager               +
            |              |           |    FROM employees                   +
            |              |           |   WHERE (employees.manager IS NULL);
(1 row)
```
Список всех отношений можно получить по команде `\\d*`, где * - символ типа объекта. Например таблицы `\\dt`, представления `\\dv`. Модификатор `+` покажет расширенную информацию. Чтобы получить детальную информацию о объекте, нужно ввести `\\d <имя_объекта>`. Модификатор `S` позволяет видеть системные объекты. `\\sf` - покажи функцию.  
Чтобы посмотреть, какие запросы выполняет psql. можно установить переменную ECHO_HIDDEN:
```
=> \set ECHO_HIDDEN on
=> \dt employees
********* QUERY **********
SELECT n.nspname as "Schema",
  c.relname as "Name",
  CASE c.relkind WHEN 'r' THEN 'table' WHEN 'v' THEN 'view' WHEN 'm' 
  THEN 'materialized view' WHEN 'i' THEN 'index' WHEN 'S' THEN 'sequence' 
  WHEN 's' THEN 'special' WHEN 'f' THEN 'foreign table' WHEN 'p' THEN 'table' END as "Type",
  pg_catalog.pg_get_userbyid(c.relowner) as "Owner"
FROM pg_catalog.pg_class c
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN ('r','p','s','')
      AND n.nspname !~ '^pg_toast'
  AND c.relname ~ '^(employees)$'
  AND pg_catalog.pg_table_is_visible(c.oid)
ORDER BY 1,2;
**************************

           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | employees | table | postgres
(1 row)

=> \unset ECHO_HIDDEN
```
OID можно преобразовывать в значение и обратно. Пример:  
```
Как мы видели, описания таблиц и представлений хранятся в pg_class. 
А столбцы располагаются в отдельной таблице pg_attribute. Чтобы получить 
список столбцов конкретной таблицы, надо соединить pg_class и pg_attribute:

=> SELECT a.attname, a.atttypid
FROM pg_attribute a
WHERE a.attrelid = (
  SELECT oid FROM pg_class WHERE relname = 'employees'
)
AND a.attnum > 0;
 attname | atttypid 
---------+----------
 id      |       23
 name    |       25
 manager |       23
(3 rows)

Используя reg-типы, запрос можно написать проще, без явного обращения к pg_class:

=> SELECT a.attname, a.atttypid
FROM pg_attribute a
WHERE a.attrelid = 'employees'::regclass
AND a.attnum > 0;
 attname | atttypid 
---------+----------
 id      |       23
 name    |       25
 manager |       23
(3 rows)

Здесь мы преобразовали строку 'employees' к типу OID. Аналогично мы можем 
вывести OID как текстовое значение:

=> SELECT a.attname, a.atttypid::regtype
FROM pg_attribute a
WHERE a.attrelid = 'employees'::regclass
AND a.attnum > 0;
 attname | atttypid 
---------+----------
 id      | integer
 name    | text
 manager | integer
(3 rows)

```
Мой разбор ниже:  
```
ВЫБЕРИ атт_имя, атт_тип ИЗ пг_атрибут ГДЕ атт_рилид = ( ВЫБЕРИ оид ИЗ пг_класс ГДЕ рил_нейм = владелец) И атт_номер > 0
ВЫБЕРИ атт_имя, атт_тип ИЗ пг_атрибут ГДЕ атт_рилид = владелец::преобразуй_в_оид И атт_номер > 0
ВЫБЕРИ атт_имя, атт_тип::преобразуй_в_значение ИЗ пг_атрибут ГДЕ атт_рилид = владелец::преобразуй_в_номер И атт_номер > 0
```
  
## Табличные пространства.  
  
Табличные пространства служат для организации физического хранения данных и определяют расположение файлов в ФС. При инициализации кластера создаются два ТП - *pg_default* и *pg_global*. В одной ТП может быть несколько БД и одна БД может быть в нескольких ТП. В *pg_default* хранятся системные объекты и таблицы, для которых не было указано ТП. В *pg_global* хранятся те объекты, которые являются общими для кластера. Хранение:  
 1. *pg_global* в `$PGDATA/global/`;
 2. *pg_default* в `$PGDATA/base/`;
 3. *пользовательская ТП* в произвольном каталоге. Для удобства pg создает на него символьную ссылку в каталоге `$PGDATA/pg_tblspc`;  
 Внутри каталога `$PGDATA/base/` данные дополнительно раскладываются по подкаталогам.  
Создание своего ТП и назначение его новой БД:  
```
=> CREATE TABLESPACE ts LOCATION '/home/postgres/ts_dir';

CREATE TABLESPACE

Список табличных пространств можно получить и командой psql:

=> \db
              List of tablespaces
    Name    |  Owner   |       Location        
------------+----------+-----------------------
 pg_default | postgres | 
 pg_global  | postgres | 
 ts         | postgres | /home/postgres/ts_dir
(3 rows)

У каждой базы данных есть табличное пространство "по умолчанию".
Создадим БД и назначим ей ts в качестве такого пространства:

=> CREATE DATABASE appdb TABLESPACE ts;

CREATE DATABASE
```
Объекты можно переносить между ТП. Для переноса одного объекта:  
```
=> ALTER TABLE t1 SET TABLESPACE pg_default;
ALTER TABLE
```
Для переноса всех объектов:  
```
=> ALTER TABLE ALL IN TABLESPACE pg_default SET TABLESPACE ts;
ALTER TABLE
```
Перенос - физическая операция переноса файлов. Перемещаемый объект блокируется.  
Полезная функция, чтобы узнать размер ТП:  
```
=> SELECT pg_size_pretty( pg_tablespace_size('ts') );
 pg_size_pretty 
----------------
 7134 kB
(1 row)
```
Чтобы удалить табличное пространство, надо написать `DROP TABLESPACE <имя>;`, но это сработает если ТП пустое. Каскад тут работать не будет. Чтобы найти все объекты, относящиеся к ТП, нужно сделать следующее.  
Сначала узнаем и запомним OID табличного пространства:
```
=> SELECT OID FROM pg_tablespace WHERE spcname = 'ts';
  oid  
-------
 16469
(1 row)

=> SELECT OID AS tsoid FROM pg_tablespace WHERE spcname = 'ts' \gset
```
Затем получим список баз данных, в которых есть объекты из удаляемого пространства:
```

=> SELECT datname
FROM pg_database
WHERE OID IN (SELECT pg_tablespace_databases(:tsoid));
 datname  
----------
 configdb
 appdb
(2 rows)
```
Дальше подключаемся к каждой базе данных и получаем список объектов из pg_class:
```
=> \c configdb
You are now connected to database "configdb" as user "postgres".
=> SELECT relnamespace::regnamespace, relname, relkind
FROM pg_class
WHERE reltablespace = :tsoid;
 relnamespace | relname | relkind 
--------------+---------+---------
 public       | t       | r
(1 row)
```
Таблица больше не нужна, удалим ее:
```
=> DROP TABLE t;
DROP TABLE
```
Смена табличного пространства, вызовет перенос всех данных из изначального табличного пространства, в перемещаемое:
```
=> ALTER DATABASE appdb SET TABLESPACE pg_default;
```
  
## Низкий уровень работы PGSQL.  
  
Каждому объекту БД, хранящему данные, соотвествует несколько слоёв. Каждый слой содержит определенный вид данных. Вначале слой содержит один файл, его имя - числовой ID, к которому может быть добавлено окончание, соотвествующее имени слоя. После роста файла до 1ГБ, создается следующий файл этого же слоя (сегмент), номер сегмента добавляется в конец имени файла.  
  
Типы слоёв:
- Основной слой - данные, версии строк таблиц или строки индексов. Имя состоит из ID без доп. окончания. Этот слой существует для любых объектов.
- Слой инициализации - имеет окончание "_init", существует только для нежурналируемых таблиц. При восстановлении PostgreSQL удаляет все страницы таких объектов и записывает слой инициализации на место основного слоя.
- Карта свободного пространства (fsm) - в ней отмечено наличие пустого места внутри страниц. Карта свободного пространства используется при вставке новых версий строк, чтобы быстро найти подходящую страницу.
- Карта видимости (vm) - битовая карта видимости, в ней отмечены страницы, которые содержат только актуальные версии строк, видимые во всех снимках данных. Карта видимости применяется для оптимизации очситки.
  
Эти файлы разделены на страницы, размер по умолчанию 8КБ. Любые страницы любых файлов сначала читаются в буферный кэш, там с ними могут работать процессы PgSQL, затем страницы вытесняются на диск. 
  
TOAST.  
Любая версия строки в PG должна помещаться на одну страницу. Если это условие не выполнимо, то используется механиз TOAST. Он подразумевает несколько стратегий: сжатие или отправка в отдельную служебную таблицу. При необходимости, для каждой основной таблицы, создается отдельная TOAST таблица. Такие таблицы распологается в схеме pg_toast.  
Чтобы узнать название файла для таблицы, относительно $PGDATA, нужно воспользоваться функцией:
```
=> SELECT pg_relation_filepath('t');
 pg_relation_filepath 
----------------------
 base/16499/16502
```
Чтобы посмотреть на файлы индексов, нужно сделать:
```
=> \d t
                            Table "public.t"
 Column |  Type   | Collation | Nullable |            Default            
--------+---------+-----------+----------+-------------------------------
 id     | integer |           | not null | nextval('t_id_seq'::regclass)
 n      | numeric |           |          | 
Indexes:
    "t_pkey" PRIMARY KEY, btree (id)

=> SELECT pg_relation_filepath('t_pkey');
 pg_relation_filepath 
----------------------
 base/16499/16509
(1 row)
```
И файлы последовательности:
```
=> SELECT pg_relation_filepath('t_id_seq');
 pg_relation_filepath 
----------------------
 base/16499/16500
(1 row)
```
Существует расширение, которое позволяет связать объекты БД и файлы:
```
postgres$ oid2name
All databases:
    Oid  Database Name  Tablespace
----------------------------------
  16499  data_lowlevel  pg_default
  12328       postgres  pg_default
  12327      template0  pg_default
      1      template1  pg_default
```
Можно посмотреть все объекты в базе:
```

postgres$ oid2name -d data_lowlevel
From database "data_lowlevel":
  Filenode  Table Name
----------------------
     16502           t
```
Или все табличные пространства в базе:
```

postgres$ oid2name -d data_lowlevel -s
All tablespaces:
   Oid  Tablespace Name
-----------------------
  1663       pg_default
  1664        pg_global
```
Можно по имени таблицы узнать имя файла:
```

postgres$ oid2name -d data_lowlevel -t t
From database "data_lowlevel":
  Filenode  Table Name
----------------------
     16502           t
```
Или наоборот, по номеру файла узнать таблицу:
```
postgres$ oid2name -d data_lowlevel -f 16502
From database "data_lowlevel":
  Filenode  Table Name
----------------------
     16502           t
```
Можно узнать размеры объектов и слоёв:
```
=> SELECT pg_relation_size('t','main') main,
pg_relation_size('t','fsm') fsm,
pg_relation_size('t','vm') vm;
  main  |  fsm  |  vm  
--------+-------+------
 409600 | 24576 | 8192
(1 row)
```
Поиск TOAST таблиц:
```
=> SELECT relname, relfilenode FROM pg_class WHERE OID = (
  SELECT reltoastrelid FROM pg_class WHERE relname='t'
);
    relname     | relfilenode 
----------------+-------------
 pg_toast_16502 |       16506
(1 row)
```
Существую несколько стратегий для работы с длинными значениями, они указываются в поле *Storage*:
```
=> \d+ t
                                                Table "public.t"
 Column |  Type   | Collation | Nullable |            Default            | Storage | Stats target | Description 
--------+---------+-----------+----------+-------------------------------+---------+--------------+-------------
 id     | integer |           | not null | nextval('t_id_seq'::regclass) | plain   |              | 
 n      | numeric |           |          |                               | main    |              | 
Indexes:
    "t_pkey" PRIMARY KEY, btree (id)
```
 - Plain - TOAST yt ghbvtyztncz/
 - Main - приоритет сжатия
 - Extended - сжатие и хранение
 - External - только хранение  
Сратегию можно изменить, если это необходимо. Изменения коснуться только новых данных:
```
=> ALTER TABLE t ALTER COLUMN n SET STORAGE extended;
ALTER TABLE
```  
  
## Вопросы.  
  
1. Был ли опыт вынесения табличных пространств на сетевое хранилище или в облако? Насколько сильно это повлияло на работу БД? Не сильно, если нормальная скорость интернета и дисков. 
2. Абстракция relation = абстракция объект? Да.  
3. Как размер страницы буферного кэша влияет на работу?
4. Влияние сжатия длинных строк, которые не помещаются в страницу, на производительность системы?
5. Если на кластере с величиной страницы 16кб сделать pg_dump и развернуть эту копию на кластере с величиной страницы в 8кб оно будет работать? Будет работать.
6. Вопрос про тип слоя "_init". В руководстве сказано, что при восстановлении после сбоя, пг удаляет этот слой и записывает данные в основной. Получается, у нас чистая таблица. Но если эта таблица работала некоторое время, в ней были сохраненные данные за какой-то период (которые вытеснялись на диск успешно), то они тоже удалятся? Сохраненные изменения сохранятся. 
7. Хочу узнать твой комментарий касательной [этой](https://simply.name/ru/pg-lc-collate.html) статьи. Делаем по ТЗ.
