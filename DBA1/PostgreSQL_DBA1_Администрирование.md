## Мониторинг работы.  
  
Существует два основных источника даннных информации о происходящем в системе:
 - статистическая информация, которая собирается PG и хранится внутри БД. Текущая активность всех обслуживающих процессов и фоновых процессов отображается в представлении *pg_stat_activity*. Существуют ещё представления, котораяе показывают текущие активности сервера. Работу этих представлений можно отключить параметром *track_activities*, но делать этого не стоит. По умолчанию включено. Кроме показа действий PG собирает некоторую статистику. Её сбором занимается фоновый процесс *stats_collector*. Чем больше параметров собирается, тем это дороже.  
 *track_counts* - обращения к таблицам и индекса, нужен для автоочистки, включен по умолчанию.  
 *track_io_timig* - обращения к страницам, по умолчанию выключен.  
 *track_functions* - вызовы пользовательских функций, выключен по умолчанию.  
   
 Каждый обслуживающий процесс собирает статистику в рамках каждой выполняемой транзакции. Затем статистика передается процессу-коллектору. Коллектор собирает и агрегирует статистику  со всех обслуживающих процессов. Раз в пол-секунды коллектор сбрасывает статистику в tmp файлы в $PGDATA/pg_stat_tmp. Когда обслуживающий процесс запрашивает информацию о статистике,  в его память читается последняя доступная версия статистики - снимок статистики. При остановке коллектор сбрасывает статистику в постоянные файлы в $PGDATA/pg_stat.  
 Существуют расширения для сбора дополнительной статистики:  
*pg_stat_statements* - статистика по запросам. Входи в поставку;  
*pgstattuple* - статистика по версиям строк. Входит в поставку;  
*pg_buffercache* - состояние буферного кэша. Входит в поставку;  
*pg_stat_plans* - статистика по планам запросов. Не входит в поставку;  
*pg_stat_kcache* - статистика по процессору и вводу-выводу. Не входит в поставку;  
*pg_qualstats* - статистика по предикатам. Не входит в поставку;  
 - журнал сообщений. Журнал сообщений можно направлять в разные приемники и выводить в разных форматах. Параметр, который это определяет - *log_destination = список*, где:  
 *stderr* - поток ошибок;  
 *csvlog* - формат CSV (только с коллектором);  
 *syslog* - демон syslog;  
 *eventlog* - журнал событий Windows;  
 Обычно дополнительно включают коллектор сообщений, он позволяет записать больше информации, собирая её со всех процессов PG. Коллектор включается параметром *logging_collector*. При значении *stderr* информация записывается в каталог, указанный в *log_directory*, в файл указанный в *log_filename*. По умолчанию почти весь вывод журнала отключен, потому что происходит много записи на диск и это может стать узким местом. Нужно самому решить, что нужно. Коллектор сообщений имеет встроенные средства ротации:  
 *log_filename* - маска имени файла;  
 *log_rotation_age* - время ротации в минутах;
 *log_rotation_size* - размер файла для ротации в КБ;  
 *log_truncate_on_rotation = on* - перезаписывать ли файлы;  
 Например `'postgresql-%H.log','1h'` - 24 файла в сутки; `'postgresql-%a.log','1d'` - 7 файлов в неделю.  
 Для анализа журнала можно использоваться средства ОС или стороние программы. Например [PgBadger](https://pgbadger.darold.net/).  
  
Примеры.  
Включим сбор статистики IO и выполнения функций:
```
=> ALTER SYSTEM SET track_io_timing=on;
ALTER SYSTEM
=> ALTER SYSTEM SET track_functions='all';
ALTER SYSTEM
=> SELECT pg_reload_conf();
 pg_reload_conf 
----------------
 t
(1 row)
```
В pg есть бенчмарк для нагрузок. Создадим таблицу и наполним её:
```
=> CREATE DATABASE admin_monitoring;
CREATE DATABASE
postgres$ pgbench -i admin_monitoring
NOTICE:  table "pgbench_history" does not exist, skipping
NOTICE:  table "pgbench_tellers" does not exist, skipping
NOTICE:  table "pgbench_accounts" does not exist, skipping
NOTICE:  table "pgbench_branches" does not exist, skipping
creating tables...
100000 of 100000 tuples (100%) done (elapsed 0.13 s, remaining 0.00 s)
vacuum...
set primary keys...
done.
```
Сброс статистики:
```
=> \c admin_monitoring
You are now connected to database "admin_monitoring" as user "postgres".
=> SELECT pg_stat_reset();
 pg_stat_reset 
---------------
 
(1 row)

=> SELECT pg_stat_reset_shared('bgwriter');
 pg_stat_reset_shared 
----------------------
 
(1 row)
```
Нагрузим таблицу:
```
postgres$ pgbench -T 10 admin_monitoring
starting vacuum...end.
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 1
number of threads: 1
duration: 10 s
number of transactions actually processed: 9304
latency average = 1.075 ms
tps = 930.308591 (including connections establishing)
tps = 930.517389 (excluding connections establishing)

=> VACUUM pgbench_accounts;
VACUUM
```
Статистика обращений к таблицам в треминах строк:
```
=> SELECT * FROM pg_stat_all_tables WHERE relid='pgbench_accounts'::regclass \gx
-[ RECORD 1 ]-------+-----------------
relid               | 16519
schemaname          | public
relname             | pgbench_accounts
seq_scan            | 0
seq_tup_read        | 0
idx_scan            | 18608
idx_tup_fetch       | 18608
n_tup_ins           | 0
n_tup_upd           | 9304
n_tup_del           | 0
n_tup_hot_upd       | 7694
n_live_tup          | 0
n_dead_tup          | 3171
n_mod_since_analyze | 9304
last_vacuum         | 
last_autovacuum     | 
last_analyze        | 
last_autoanalyze    | 
vacuum_count        | 0
autovacuum_count    | 0
analyze_count       | 0
autoanalyze_count   | 0
```
В терминах страниц:
```
=> SELECT * FROM pg_statio_all_tables WHERE relid='pgbench_accounts'::regclass \gx
-[ RECORD 1 ]---+-----------------
relid           | 16519
schemaname      | public
relname         | pgbench_accounts
heap_blks_read  | 25
heap_blks_hit   | 34524
idx_blks_read   | 221
idx_blks_hit    | 40405
toast_blks_read | 
toast_blks_hit  | 
tidx_blks_read  | 
tidx_blks_hit   | 
```
Cуществуют аналогичные представления для индексов:
```
=> SELECT * FROM pg_stat_all_indexes WHERE relid='pgbench_accounts'::regclass \gx
-[ RECORD 1 ]-+----------------------
relid         | 16519
indexrelid    | 16530
schemaname    | public
relname       | pgbench_accounts
indexrelname  | pgbench_accounts_pkey
idx_scan      | 18608
idx_tup_read  | 20329
idx_tup_fetch | 18608

=> SELECT * FROM pg_statio_all_indexes WHERE relid='pgbench_accounts'::regclass \gx
-[ RECORD 1 ]-+----------------------
relid         | 16519
indexrelid    | 16530
schemaname    | public
relname       | pgbench_accounts
indexrelname  | pgbench_accounts_pkey
idx_blks_read | 221
idx_blks_hit  | 40405
```
Статистика по всей БД:
```
=> SELECT * FROM pg_stat_database WHERE datname='admin_monitoring' \gx
-[ RECORD 1 ]--+-----------------------------
datid          | 16512
datname        | admin_monitoring
numbackends    | 1
xact_commit    | 9314
xact_rollback  | 0
blks_read      | 330
blks_hit       | 121986
tup_returned   | 124242
tup_fetched    | 18782
tup_inserted   | 9304
tup_updated    | 27913
tup_deleted    | 0
conflicts      | 0
temp_files     | 0
temp_bytes     | 0
deadlocks      | 0
blk_read_time  | 4.578
blk_write_time | 0
stats_reset    | 2019-03-31 15:25:42.27966+03
```
Статистика по чекпоинтам:
```
=> CHECKPOINT;
CHECKPOINT
=> SELECT * FROM pg_stat_bgwriter \gx
-[ RECORD 1 ]---------+------------------------------
checkpoints_timed     | 0
checkpoints_req       | 1
checkpoint_write_time | 35
checkpoint_sync_time  | 23
buffers_checkpoint    | 1953
buffers_clean         | 0
maxwritten_clean      | 0
buffers_backend       | 1700
buffers_backend_fsync | 0
buffers_alloc         | 391
stats_reset           | 2019-03-31 15:25:42.329588+03

buffers_clean - количество страниц, записанных фоновой записью;
buffers_checkpoint - количество страниц, записанных контрольной точкой;
buffers_backend - количество страниц, записанных серверными процессами.
```
  
Ситуация: как удалить процесс, который не завершил транзакцию и поставил блокировку.  
С версии 9.6 есть параметр *idle_in_transaction_session_timeout* - принудительно завершает сеансы в которых транзакция простаивает дольше заданного времени. Сейчас мы удалим его вручную.  
Для начала посмотрим информацию о обслуживающих процессах:
```
=> SELECT pid, query, state, wait_event, wait_event_type, pg_blocking_pids(pid)
FROM pg_stat_activity
WHERE backend_type = 'client backend' \gx
-[ RECORD 1 ]----+-----------------------------------------------------------------------------
pid              | 30562
query            | UPDATE t SET n = n + 1;
state            | idle in transaction
wait_event       | ClientRead
wait_event_type  | Client
pg_blocking_pids | {}
-[ RECORD 2 ]----+-----------------------------------------------------------------------------
pid              | 30022
query            | SELECT pid, query, state, wait_event, wait_event_type, pg_blocking_pids(pid)+
                 | FROM pg_stat_activity                                                       +
                 | WHERE backend_type = 'client backend' 
state            | active
wait_event       | 
wait_event_type  | 
pg_blocking_pids | {}
-[ RECORD 3 ]----+-----------------------------------------------------------------------------
pid              | 30614
query            | UPDATE t SET n = n + 2;
state            | active
wait_event       | transactionid
wait_event_type  | Lock
pg_blocking_pids | {30562}
```
Нам нужно удалить pid 30562. pid 30614 ждет снятия блокировки. Запомним pid заблокированного процесса:
```
=> SELECT pid as blocked_pid
FROM pg_stat_activity
WHERE backend_type = 'client backend'
AND cardinality(pg_blocking_pids(pid)) > 0 \gset
```
(хотя можно было просто \set blocked_pid 30614)  
Далее вызваем на блокировщика функцию *pg_terminate_backend*:
```
=> SELECT pg_terminate_backend(b.pid)
FROM unnest(pg_blocking_pids(:blocked_pid)) AS b(pid);
 pg_terminate_backend 
----------------------
 t
(1 row)
```
Существует предлставление, которое показывает инфу о служебных фоновых процессах экземпляра:
```
=> SELECT pid, backend_type, backend_start, state
FROM pg_stat_activity;
  pid  |    backend_type     |         backend_start         | state  
-------+---------------------+-------------------------------+--------
 22616 | background worker   | 2019-03-31 15:25:22.337945+03 | 
 22614 | autovacuum launcher | 2019-03-31 15:25:22.339051+03 | 
 30022 | client backend      | 2019-03-31 15:25:42.252197+03 | active
 30614 | client backend      | 2019-03-31 15:25:54.465374+03 | idle
 22612 | background writer   | 2019-03-31 15:25:22.340118+03 | 
 22611 | checkpointer        | 2019-03-31 15:25:22.340522+03 | 
 22613 | walwriter           | 2019-03-31 15:25:22.339701+03 | 
(7 rows)
```
Очень полезная настройка - настройка префикса журнала. Все параметры можно найти на сайте PG, а пока пример:
```
=> ALTER SYSTEM SET log_min_duration_statement=0;
ALTER SYSTEM
=> ALTER SYSTEM SET log_line_prefix='(pid=%p) ';
ALTER SYSTEM
=> SELECT pg_reload_conf();
 pg_reload_conf 
----------------
 t
(1 row)
```
  
## Сопровождение.  
  
Для всех баз существуют определенные сервсиные операции, которые мы рассмотрим.  
Процедура очистки страниц. Очистка нужна для удаления старых версий строк в страницах. Она работает параллельно с другими транзакциями, обрабатывая файлы постранично, она не уменьшает размер файлов. Вместо этого в файлах образуется свободное пространство. Очистку можно запускать руками `vacuum таблица`, `vacuum`, а можно автоматически. Для автоматической очистки используется процесс *autovacuum*. Она реагирует на частоту обновлений страниц прямо пропорционально. В системе постоянно присутствует процесс *autovacuum launcher*, который планирует работу очисти и запускает необходимо число *autovacuum worker*, работающих параллельно. Также существует полная очистка `vacuum full таблица`, `vacuum full`. Этот процесс полностью перезаписывает содержимое страниц и индексов, минимизируя занимаемое место, однако он требует эксклюзивной блокировки. Есть стороннее расширение *pg_repack*, позволяющее выполнить перестроение таблиц и её индексов "на лету".  
Для правильной работы оптимизатора необходима актуальная статистика. Процесс *autovacuum* помимо очистки, собирает статистику. Если этого мало, можно сделать `analyze` или `vacuum analyze`.  
Переполнение и заморозка. Пространство номеров транзакций закольцовано. Если существуют транзакции, которые видны уже на всех снимках данных, помечаются как "замороженные". Тогда механизм многоверсиооности не смотрит на номера таких транзакций. Заморозку выполняет *autovacuum*. Если сервер встречает не замороженную страницу, то работа сервера останавливается и ждет, пока администратор руками не запустит его и не сделает очистку.  
Индексы. Следует отслеживать их состояние и при необходимости удалять ненужные и перестраивать существующие. Для перестроение индексов существую следующие команды:
 - `REINDEX INDEX` - перестроить индекс;
 - `REINDEX TABLE` - перестроить все индексы таблицы;
 - `REINDEX SCHEMA` - перестроить все индексы схемы;
 - `REINDEX DATABASE` - перестроить все индексы текущей БД;
 - `REINDEX SYSTEM` - перестроить только системные индексы;
 - `VACUUM FULL` - вместе с таблицей перестаривает индексы.  
Перестраивание устанавливает эксклюзивную блокировку. Индексы можно перестроить вручную.  
Примеры оценки разрастания таблиц и индексов.  
Для оценки можно воспользоваться расширением *pgstattuple*;
```
=> CREATE EXTENSION pgstattuple;
CREATE EXTENSION
```
Создадим таблицу и заполним её данными:
```
=> CREATE TABLE bloat(id serial, s text);
CREATE TABLE
=> INSERT INTO bloat(s)
  SELECT g.id::text FROM generate_series(1,100000) AS g(id);
INSERT 0 100000
=> CREATE INDEX ON bloat(s);
CREATE INDEX
```
Проверим состояние таблицы:
```
=> SELECT * FROM pgstattuple('bloat') \gx
-[ RECORD 1 ]------+--------
table_len          | 4014080
tuple_count        | 100000
tuple_len          | 3388895
tuple_percent      | 84.43
dead_tuple_count   | 0
dead_tuple_len     | 0
dead_tuple_percent | 0
free_space         | 4356
free_percent       | 0.11

tuple_percent - доля полезной информации (не 100% из-за накладных расходов).
```
И индекса:
```
=> SELECT * FROM pgstatindex('bloat_s_idx') \gx
-[ RECORD 1 ]------+--------
version            | 2
tree_level         | 1
index_size         | 2252800
root_block_no      | 3
internal_pages     | 1
leaf_pages         | 273
empty_pages        | 0
deleted_pages      | 0
avg_leaf_density   | 89.98
leaf_fragmentation | 0

avg_leaf_density - заполненность листовых страниц индекса;
leaf_fragmentation - характеристика физической упорядоченности страниц.
```
Обновим половину строк:
```
=> UPDATE bloat SET s = s || '!' WHERE id % 2 = 0;
UPDATE 50000
```
Снова посмотрим на таблицу и индексы:
```
=> SELECT * FROM pgstattuple('bloat') \gx
-[ RECORD 1 ]------+--------
table_len          | 6021120
tuple_count        | 100000
tuple_len          | 3438895
tuple_percent      | 57.11
dead_tuple_count   | 50000
dead_tuple_len     | 1694450
dead_tuple_percent | 28.14
free_space         | 4732
free_percent       | 0.08

=> SELECT * FROM pgstatindex('bloat_s_idx') \gx
-[ RECORD 1 ]------+--------
version            | 2
tree_level         | 2
index_size         | 4505600
root_block_no      | 412
internal_pages     | 3
leaf_pages         | 546
empty_pages        | 0
deleted_pages      | 0
avg_leaf_density   | 67.6
leaf_fragmentation | 49.82
```
  
## Вопросы.  
1. Не очень понял термины "обращение к таблицам в треминах строк" и "термины страниц".
2. Насколько часто бывают проблемы с заморозкой и бывают ли вообще? Не было.
3. Не очень понимаю работу с индексами. В частности, как вручную сделать корректный индекс, который действительно будет ускорять работу и как понять, что индекс ненужный? Правильно ли я понимаю, что для этого нужно собрать статистику запросов к таблице и, исходя из её анализа, сделать индексы для самых часто используемых запросов? Да, все верно.
