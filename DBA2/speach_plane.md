## План рассказа. 
1. Зайти в папку pg и показать на файл конфигурации сервера, базово рассказать о параметрах.
2. Зайти в папку pg и показать на файл для подключений к серверу, базово рассказать о параметрах.
3. Рассказать в общем о каталогах в папке. Подсказка ниже:  
```
PG_VERSION	Файл, содержащий номер основной версии PostgreSQL
base	Подкаталог, содержащий подкаталоги для каждой базы данных
current_logfiles	Файл, в котором отмечается, в какие файлы журналов производит запись сборщик сообщений
global	Подкаталог, содержащий общие таблицы кластера, такие как pg_database
pg_commit_ts	Подкаталог, содержащий данные о времени фиксации транзакций
pg_dynshmem	Подкаталог, содержащий файлы, используемые подсистемой динамически разделяемой памяти
pg_logical	Подкаталог, содержащий данные о состоянии для логического декодирования
pg_multixact	Подкаталог, содержащий данные о состоянии мультитранзакций (используемые для разделяемой блокировки строк)
pg_notify	Подкаталог, содержащий данные состояния прослушивания и нотификации (LISTEN/NOTIFY)
pg_replslot	Подкаталог, содержащий данные слота репликации
pg_serial	Подкаталог, содержащий информацию о выполненных сериализуемых транзакциях.
pg_snapshots	Подкаталог, содержащий экспортированные снимки (snapshots)
pg_stat	Подкаталог, содержащий постоянные файлы для подсистемы статистики.
pg_stat_tmp	Подкаталог, содержащий временные файлы для подсистемы статистики
pg_subtrans	Подкаталог, содержащий данные о состоянии подтранзакций
pg_tblspc	Подкаталог, содержащий символические ссылки на табличные пространства
pg_twophase	Подкаталог, содержащий файлы состояний для подготовленных транзакций
pg_wal	Подкаталог, содержащий файлы WAL (журнал предзаписи)
pg_xact	Подкаталог, содержащий данные о состоянии транзакции
postgresql.auto.conf	Файл, используемый для хранения параметров конфигурации, которые устанавливаются при помощи ALTER SYSTEM
postmaster.opts	Файл, содержащий параметры командной строки, с которыми сервер был запущен в последний раз
postmaster.pid	Файл блокировки, содержащий идентификатор (ID) текущего управляющего процесса (PID), путь к каталогу данных  
кластера, время запуска управляющего процесса, номер порта, путь к каталогу Unix-сокета (пустой для Windows), первый корректный  
адрес прослушивания (listen_address) (IP-адрес или *, либо пустое значение в случае отсутствия прослушивания по TCP), и ID  
сегмента разделяемой памяти (этот файл отсутствует после остановки сервера).
```
4. Через `sudo ps -ef | grep postgres` показать процессы, которые есть в системе для запуска pg.
5. Подробно рассказать о процессах, что они делают и зачем они нужны. Подсказка по [ссылке](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA1/PostgreSQL_DBA1_architecture.md#%D0%BC%D0%B5%D1%85%D0%B0%D0%BD%D0%B8%D0%B7%D0%BC%D1%8B-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%8B-%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%B0-postgresql). [autovacuum](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA2/PostgreSQL_DBA2_versions.md#%D0%B0%D0%B2%D1%82%D0%BE%D0%BE%D1%87%D0%B8%D1%81%D1%82%D0%BA%D0%B0); [WAL](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA2/PostgreSQL_DBA2_journal.md#%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0-%D0%B6%D1%83%D1%80%D0%BD%D0%B0%D0%BB%D0%B0-wal); [checkpointer](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA2/PostgreSQL_DBA2_journal.md#%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%BE%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D1%82%D0%BE%D1%87%D0%BA%D0%B0); [statscollector](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA2/PostgreSQL_DBA2_versions.md#%D0%B0%D0%B2%D1%82%D0%BE%D0%BE%D1%87%D0%B8%D1%81%D1%82%D0%BA%D0%B0);
6. Далее захожу в кластер и рассказываю про то что такое кластер БД. Кластер - несколько БД под управлением одного экземпляра сервера.
7. Рассказываю про [табличные пространства](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA1/PostgreSQL_DBA1_org_data.md#%D1%82%D0%B0%D0%B1%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B5-%D0%BF%D1%80%D0%BE%D1%81%D1%82%D1%80%D0%B0%D0%BD%D1%81%D1%82%D0%B2%D0%B0). \db
8. Рассказываю про [схемы](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA1/PostgreSQL_DBA1_org_data.md#%D0%B1%D0%B0%D0%B7%D1%8B-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85-%D0%B8-%D1%81%D1%85%D0%B5%D0%BC%D1%8B). \dn [Работа со схемами по ссылке](https://edu.postgrespro.ru/dba1/dba1_07_data_databases.html)
9. Рассказываю про временные объекты. \dn \dt
10. Рассказываю про страницы видимости и TOAST таблицы. [подсказка](https://git.sanjesss.tk/sanjesss/alfa-tasks/-/blob/master/DBA1/PostgreSQL_DBA1_org_data.md#%D0%BD%D0%B8%D0%B7%D0%BA%D0%B8%D0%B9-%D1%83%D1%80%D0%BE%D0%B2%D0%B5%D0%BD%D1%8C-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%8B-pgsql)  
11. Демонстрация поиска файлов страниц видимости:
    Создаем БД, таблицу и добавляем в неё значения:
    ```
    => CREATE DATABASE data_lowlevel;
    CREATE DATABASE
    => \c data_lowlevel
    You are now connected to database "data_lowlevel" as user "postgres".
    => CREATE TABLE t(id serial PRIMARY KEY, n numeric);
    CREATE TABLE
    => INSERT INTO t(n) SELECT g.id FROM generate_series(1,10000) AS g(id);
    INSERT 0 10000
    ```
    Чтобы узнать пути относительно $PGDATA воспользуемся функцией:
    ```
    => SELECT pg_relation_filepath('t');
    pg_relation_filepath 
    ----------------------
    base/16499/16502
    ```
    Будут файлы без страницы VM. Чтобы она появилась, нужно сделать `vacuum t`.  
    Также можно посмотреть на файлы индекса и последовательности:
    ```
    => \d t
                            Table "public.t"
    Column |  Type   | Collation | Nullable |            Default            
    -------+---------+-----------+----------+-------------------------------
    id     | integer |           | not null | nextval('t_id_seq'::regclass)
    n      | numeric |           |          | 
    Indexes:
    "t_pkey" PRIMARY KEY, btree (id)

    => SELECT pg_relation_filepath('t_pkey');
    pg_relation_filepath 
    ----------------------
    base/16499/16509
    (1 row)

    postgres$ ls -l /usr/local/pgsql/data/base/16499/16509*
    -rw------- 1 postgres postgres 196608 мар 31 15:25 /usr/local/pgsql/data/base/16499/16509
    ```
    Чтобы посмотреть обратно нужно воспользоваться расширением oid2name. Оно запускается из CLI UNIX!!!!!!!!!!!
    ```
    Можно посмотреть все базы данных:

    postgres$ oid2name
    All databases:
    Oid  Database Name  Tablespace
    ----------------------------------
    16499  data_lowlevel  pg_default
    12328       postgres  pg_default
    12327      template0  pg_default
    1      template1  pg_default
    Можно посмотреть все объекты в базе:

    postgres$ oid2name -d data_lowlevel
    From database "data_lowlevel":
    Filenode  Table Name
    ----------------------
    16502           t
    Или все табличные пространства в базе:

    postgres$ oid2name -d data_lowlevel -s
    All tablespaces:
    Oid  Tablespace Name
    -----------------------
    1663       pg_default
    1664        pg_global
    Можно по имени таблицы узнать имя файла:

    postgres$ oid2name -d data_lowlevel -t t
    From database "data_lowlevel":
    Filenode  Table Name
    ----------------------
    16502           t
    Или наоборот, по номеру файла узнать таблицу:

    postgres$ oid2name -d data_lowlevel -f 16502
    From database "data_lowlevel":
    Filenode  Table Name
    ----------------------
    16502           t
    ```
    Подсказка по [ссылке](https://edu.postgrespro.ru/dba1/dba1_10_data_lowlevel.html)
12. Демонстрация поиска блокировок:  
    Создаем таблицу и значение в ней:
    ```
    => CREATE TABLE t(n integer);
    CREATE TABLE
    => INSERT INTO t VALUES(42);
    INSERT 0 1
    ```
    Запускаем транзакцию:
    ```
    => BEGIN;
    BEGIN
    => UPDATE t SET n = n + 1;
    UPDATE 1
    ```
    Запускаем соседний сеанс и пытаемся изменить блокированную строку:
    ```
    => UPDATE t SET n = n + 2;
    ```
    В третьем сеансе смотрим инфу по заблокированным транзакциям:
    ```
    => SELECT pid, query, state, wait_event, wait_event_type, pg_blocking_pids(pid)
    FROM pg_stat_activity
    WHERE backend_type = 'client backend' \gx
    ```
    Записывем переменную блокированного сеанса:
    ```
    SELECT pid as blocked_pid
    FROM pg_stat_activity
    WHERE backend_type = 'client backend'
    AND cardinality(pg_blocking_pids(pid)) > 0 \gset
    ```
    Останавливаем заблокированную транзакцию:
    ```
    => SELECT pg_terminate_backend(b.pid)
    FROM unnest(pg_blocking_pids(:blocked_pid)) AS b(pid);
    ```
    Подсказка по [ссылке](https://edu.postgrespro.ru/dba1/dba1_11_admin_monitoring.html)
13. Демонстрация работы с расширениями:
    Список доступных расширений доступен по команде:
    ```
    SELECT name, default_version, installed_version
    FROM pg_available_extensions ORDER BY name;
    ```
    Установка расширения:
    ```
    Create extension *name*;
    ```
    Удаление расширения: 
    ```
    Drop extension *name*;
    ```
    Подсказка по [ссылке](https://edu.postgrespro.ru/dba2/dba2_15_admin_extensions.html)
14. Демонстрация обновления сервера.
    Сначала обновляемся с 10.5 до 10.7. Оба дистра уже должны быть установлены и на 10.5 должны быть показаны всякие штуки.
    Чтобы обновиться до следующей минорной версии достаточно просто запустить новый дистр с кластером старого. Для этого останавливаю старый кластер:
    ```
    /usr/local/pgsql-10.5/bin/pg_ctl -D /usr/local/pgsql-10.5/data -l /usr/local/pgsql-10.5/logfile stop
    ```
    И запускаю новый со старым кластером БД:
    ```
    /usr/local/pgsql-10.7/bin/pg_ctl -D /usr/local/pgsql-10.5/data -l /usr/local/pgsql-10.7/logfile start
    ```
    Обновление минорной версии заврешено.  
    Чтобы обновится до следующей мажорной версии нужно установить мажорную версию, инициализировать кластер:
    ```
    /usr/local/pgsql-11.9/bin/initdb -D /usr/local/pgsql-11.9/data -k
    ```
    Далее делаем проверку pg_upgrade:
    ```
    /usr/loca/pgsql-11.9/bin/pg_upgrade --check -b /usr/local/pgsql-10.7/bin/ -B /usr/local/pgsql-11.9/bin/ -d /usr/local/pgsql-10.5/data/ -D /usr/local/pgsql-11.9/data
    ```
    Получим ошибку о том, что у нас не установлено расширение, которое есть в старой версии. Для этого захожу в директорию contrib версии 11.9 и делаю make install. Далее снова делаю чек и все должно быть без ошибок. Далее снова запускаю pg_upgrade без ключа --check и обновляюсь. Подсказка по [ссылке](https://edu.postgrespro.ru/dba2/dba2_17_admin_upgrade.html)
