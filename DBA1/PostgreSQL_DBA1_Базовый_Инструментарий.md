## Установка и управление сервером.

Postgres можно установить из репозиториев ОС и из исходного кода. Установка из репо проста и понятна, её не стоит описывать. Опишу установку из исходного кода.  
  
После загрузки исходного кода, запускаем файл `./configure`. Далее необходимо собрать образ. Это можно сделать двумя способами: `make` и `make world`. Вторая команда соберет сервер, все расширения и документацию. Далее нам необходимо, если мы собираем из исходного кода, создать юзера `postgres`, создать директорию `/usr/local/pgsql/data` и сменить владельца этой директории на `postgres`. Также эта директория указывается в переменной `$PGDATA`. Также в `$PATH` нужно прописать путь `/usr/local/pgsql/bin` для того, чтобы не указывать полный путь до исполняемых файлов.  
  
После установки нам необходимо инициализировать кластер БД. Для этого используется утилита `initdb`. Добавим к ней ключ `-k`, он включает подсчёт контрольной суммы страниц для обнаружения повреждений данных.  
```
postgres$ initdb -k
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locales
  COLLATE:  en_US.UTF-8
  CTYPE:    en_US.UTF-8
  MESSAGES: en_US.UTF-8
  MONETARY: ru_RU.UTF-8
  NUMERIC:  ru_RU.UTF-8
  TIME:     ru_RU.UTF-8
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are enabled.

fixing permissions on existing directory /usr/local/pgsql/data ... ok
creating subdirectories ... ok
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting dynamic shared memory implementation ... posix
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

WARNING: enabling "trust" authentication for local connections
You can change this by editing pg_hba.conf or using the option -A, or
--auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    pg_ctl -D /usr/local/pgsql/data -l logfile start
```  
  
После выполнения инициализации кластера pg предлагает запустить нам сервер БД: `pg_ctl -w -D /usr/local/pgsql/data -l logfile start`. В ключе `-D` мы узазываем место для хранения данных, если переменная `$PGDATA` назначена, то можно не указывать этот параметр. Ключ `-w` означает, что к нам вернется управление только после окончания запуска сервера. В ключе `-l` мы укажем директорию для хранения системных журналов (логов).  
  
Проверить работу сервер можно, если вызвать одну из базовых функций, которая возвращает время:
```
postgres$ psql -c 'select now();'
              now              
-------------------------------
 2019-03-31 15:25:15.735019+03
(1 row)
```  
  
Чтобы установить расширения из исходного кода, необходимо в каталоге расширения сделать `make && make install`. Если хотим установить все расширения, которые доступны в каталоге, то нужно сделать `make && make install` в общем каталоге расширений `*/contrib`. Для "активации" расширений необходимо сделать `CREATE EXTENSION` в базе. Список доступных расширений можно посмотреть по команде `psql -c 'SELECT name, comment FROM pg_available_extensions ORDER BY name;'`.  
  
Для остановки сервера нужно воспользоваться командой `pg_ctl stop -m fast|smart|immediate`.  
Можно указать один из трех типов остановки:
- `fast` - принудительное завершение сеансов и запись данных из ОЗУ на диск.
- `smart` - ожидание завершения сеансов и запись данных из ОЗУ на диск.
- `immediate` - принудитеьное завершение сеансов и остановки сервера, при запуске требует восстановления.  
По умолчанию используется режим `fast`.
  
## Использование psql.  
  
psql - Терминальный клиент для работы с СУБД. Чтобы подключиться к базе через psql, нужно ввести команду `psql -d база -U роль/имя -h узел -p порт`. Чтобы подключиться к базе из psql нужно ввести `\\c база роль/имя узел порт`. `\\conninfo` - информация о текущем подключении.  
  
psql умеет выводить результат запроса в разных форматах. Пример обычного запроса:
```
=> SELECT schemaname, tablename, tableowner FROM pg_tables LIMIT 5;
 schemaname |    tablename    | tableowner 
------------+-----------------+------------
 pg_catalog | pg_statistic    | postgres
 pg_catalog | pg_user_mapping | postgres
 pg_catalog | pg_policy       | postgres
 pg_catalog | pg_authid       | postgres
 pg_catalog | pg_subscription | postgres
(5 rows)
```  
Переключатель `\\a` отвечает за выравнивание, `\\t` за отображение строки заголовка и итоговой строки, `\\x` расширенный формат, удобен, если нужно вывести много столбцов для малого количества записей, для использования в одном запросе следует в конце, вместо `;` указать `\\gx`. `\\pset fieldsep 'символ_разделителя'` позволяет изменить стиль разделителя. Другие возомжности форматирования доступны по `\\pset`.  
Чтобы выполнить __shell__ запрос, нужно воспользоваться `!`:
```
=> \! uptime
 15:25:23 up  6:26,  3 users,  load average: 1,27, 0,55, 0,37
```  
psql имеет переменные, также как shell. Чтобы обозначить переменную нужно ввести следующую команду `\\set TEST Hello`, чтобы получить значение пишем `\\echo :TEST`. Название переменной критично к регистру, получение значения только через `:`. Полезная функция - запись результата в переменную:  
```
=> SELECT now() AS curr_time \gset
=> \echo :curr_time
2019-03-31 15:25:23.68208+03
```   
Вызов `\\set` без параметров вернёт список переменных.  
  
Можно использовать условные операторы. Сформируем переменную, которая определяет, версия postgres больше 10 или меньше:
```
=> SELECT :SERVER_VERSION_NUM >= 100000 AS pgsql10\gset
=> \echo pgsql10: :pgsql10
pgsql10: t
```
Далее используем логическую переменную в условном операторе:
```
=> \if :pgsql10
   \! echo "WAL size:";du -h -s $PGDATA/pg_wal
\else
   \! echo "WAL size:";du -h -s $PGDATA/pg_xlog
\endif
WAL size:
17M	/usr/local/pgsql/data/pg_wal
```
  
С помощью команд, которые, в основном начинаются нс `\\d` можно смотреть информацию о объектах БД:
```
=> \d pg_tables
              View "pg_catalog.pg_tables"
   Column    |  Type   | Collation | Nullable | Default 
-------------+---------+-----------+----------+---------
 schemaname  | name    |           |          | 
 tablename   | name    |           |          | 
 tableowner  | name    |           |          | 
 tablespace  | name    |           |          | 
 hasindexes  | boolean |           |          | 
 hasrules    | boolean |           |          | 
 hastriggers | boolean |           |          | 
 rowsecurity | boolean |           |          | 
```
  
У psql есть файлы настройкии окружения. Файл для пользователя находится в домашнем каталоге пользователя: `.psqlrc`. Если прописать команду в переменную в этом файле, то она всегда будет доступна по имени переменной.  
  
## Конфигурирование сервера.  
  
У сервера pg есть много параметров, которые можно установить. Некоторые параметры можно устанавливать отдельно для БД, пользователя или всего сервера в целом. Также их можно устанавливать прямо во время сеанса.  
Основной файл конфигурации читается при старте сервера. Если один параметр указан в нем несколько раз, то применяется последнее значение. Чтобы узнать, где находится конфигурационный файл, необходимо прописать команду `SHOW config_file;`.
```
=> SELECT setting FROM pg_settings WHERE name = 'config_file';
                setting                
---------------------------------------
 /usr/local/pgsql/data/postgresql.conf
(1 row)
```
Некоторые параметры можно обновить, перечитав конфигурацию, а некоторые требуют перезагрузки сервера. Чтобы перечитать конфигурацию нужно прописать `pg_ctl reload` или в psql `SELECT pg_reload_conf();`. Чтобы изменить конфигураию прямо в psql можно воспользоваться комнадой `ALTER SYSTEM`.  
```
ALTER SYSTEM SET параметр TO значение; - добавляет или изменяет строку.
ALTER SYSTEM RESET параметр; - удаляет строку.
ALTER SYSTEM RESET ALL; - удаляет все строки.
```
Эти команды изменяют параметры в файле `postgresql.auto.conf`, который считывается в последнюю очередь. Этот файл всегда распологается в $PGDATA.  
  
В стандартном конфигурационном файле много закоменнтированных строк. Чтобы увидеть параметры, которые применяются в данный момент, нужно воспользоваться следующей конструкцией:
```
=> SELECT sourceline, name, setting, applied
   FROM   pg_file_settings
   WHERE  sourcefile LIKE '%postgresql.conf';
 sourceline |            name            |      setting       | applied 
------------+----------------------------+--------------------+---------
         64 | max_connections            | 100                | t
        113 | shared_buffers             | 128MB              | t
        127 | dynamic_shared_memory_type | posix              | t
        472 | log_timezone               | W-SU               | t
        566 | datestyle                  | iso, dmy           | t
        568 | timezone                   | W-SU               | t
        581 | lc_messages                | en_US.UTF-8        | t
        583 | lc_monetary                | ru_RU.UTF-8        | t
        584 | lc_numeric                 | ru_RU.UTF-8        | t
        585 | lc_time                    | ru_RU.UTF-8        | t
        588 | default_text_search_config | pg_catalog.english | t
(11 rows)
```
Где столбец *applied* означает, нужно ли перезапускать сервер после применения этого параметра.  
  
Чтобы получить значение параметра из psql нужно воспользоваться следующей конструкцией:
```
=> SELECT name, setting, unit,
          boot_val, reset_val,
          source, sourcefile, sourceline,
          pending_restart, context
   FROM   pg_settings WHERE name = 'work_mem'\gx
-[ RECORD 1 ]---+---------
name            | work_mem
setting         | 4096
unit            | kB
boot_val        | 4096
reset_val       | 4096
source          | default
sourcefile      | 
sourceline      | 
pending_restart | f
context         | user
```
Где name, setting и unit - название, значение и мера измерения параметра. Boot_val - значение по умолчанию при загрузке, reset_val - значение, которе мы получим, если сделаем `ALTER SYSTEM RESET`. Source - источник текущего параметра. Pending_restart - ожидает ли  рестарта сервера для применения значения. Context - как можно применить этот параметр: internal - изменить нельзя, задано при установке, postmaster - требуется рестарт сервера, sighup - требуется перечитать конфиг, superuser - только su может изменить, user - можно изменить для своего сеанса.  
  
Чтобы изменить параметр прямо во время сеанса, нужно воспользоваться конструкцией:
```
=> SET work_mem TO '24MB';
SET
```
Или функцию set_config:
```
=> SELECT set_config('work_mem', '32MB', false);
 set_config 
------------
 32MB
(1 row)
```
Где третий параметр (false), говорит о том, нужно ли устанавливать значение только для текущей транзакции (true) или до конца сеанса (false)
# Вопросы.  
  
 1. Можно ли изменить параметр безопасности contex у некоторых параметров? Например, work_mem может изменять любой юзер в своём сеансе. Как можно разрешить изменение этого параметра только su? Без ответа.
 2. Объяснить, что такое пул соединений, очень коротко. Одиссей, пул соединений.
 3. В лекции есть часть про транзакцию:
 ```
Установка параметров транзакции
Откроем транзакцию и установим новое значение work_mem:

=> RESET work_mem;
RESET
=> BEGIN;
BEGIN
=> SET work_mem TO '64MB';
SET
=> SHOW work_mem;
 work_mem 
----------
 64MB
(1 row)

Если транзакция откатывается, то установка параметра отменяется:

=> ROLLBACK;
ROLLBACK
=> SHOW work_mem;
 work_mem 
----------
 8MB
(1 row)
 ```
 Вопрос: для чего впринципе открывать транзакцию таким способом? Ведь если я воспользуюсь операцией INSERT то это тоже транзакция. Чтобы проводить транзакцию с измененными параметрам и все? В основном нами не используется. Это вопрос к ПО.
 4. В лекции рассказано про пользовательские параметры:
 ```
Параметры можно создавать прямо во время сеанса, в том числе с предварительной проверкой на существование:

=> SELECT CASE WHEN current_setting('myapp.currency_code', true) IS NULL THEN
                set_config('myapp.currency_code', 'RUB', false)
            ELSE
                current_setting('myapp.currency_code')
            END;
 current_setting 
-----------------
 RUB
(1 row)

В имени пользовательского параметра обязательно должна быть точка, чтобы отличать их от стандартных параметров.

Теперь myapp.currency_code можно использовать как глобальную переменную сеанса:

=> SELECT current_setting('myapp.currency_code');
 current_setting 
-----------------
 RUB
(1 row)

Пользовательские параметры можно указать в postgresql.conf, тогда они автоматически будут инициализироваться во всех сеансах.
 ```
Проверка на существование - есть ли уже такой параметр?  Да.
Эти пользовательские параметры это, получается, просто как пара ключ=значение, которые ничего не изменяют в работе сервера?  
Для чего они применяются?  
  
## Внеклассное чтиво:

Представление - виртуальная таблица, которая на самом деле является запросом. Есть много системных представлений, их список можно получить в psql командой `\\dvS` .
