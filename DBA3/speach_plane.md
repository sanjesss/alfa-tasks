### Логические бекапы.  
  
COPY.
`COPY table TO 'file';`
`COPY table FROM 'file';`
Работает из psql. Сначала создаем таблицу, потом работаем с ней. Для загрузки файлов в таблицу она должна быть создана.  
  
pg_dump.  
Делаем бекап одной базы. Делается из под юзера postgres `pg_dump -d base -f file.sql`. Восстановление через `psql -d base -f file.sql`.  
Далее pg_dump в формате [кастом](https://gitlab.ru/sanjesss/alfa-tasks/-/blob/master/DBA3/PostgreSQL_DBA3_backup.md#%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F-%D0%B1%D0%B0%D0%B7%D1%8B-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%8). Такой бекап делается через `pg_dump --format=custom -d base -f file.custom`. Восстановить нужно через `pg_restore -d db2 file.custom`. Посмотреть лист бекапа и отредактировать что нужно восстановаливать можно, если открыть `pg_restore --list file.custom`. Выводим это в файл, редактируем и используем через `--use-list`.  
  
Далее pg_dump в формате [directory](https://gitlab.ru/sanjesss/alfa-tasks/-/blob/master/DBA3/PostgreSQL_DBA3_backup.md#%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F-%D0%B1%D0%B0%D0%B7%D1%8B-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%8). Используем `pg_dump --format=directory -d base -f /file.directory`. Восстанавливаться также через `pg_restore`. Ключ `--jobs=n` где n= количество потоков. `pg_restore --use-list list.list db.custom -d db` 
  
pg_dumpall.  
Делаем копию всего кластера через `pg_dumpall --clean -U postgres -f file.sql`. Работает только в однопоточном режиме и нужно через SU. Если в однопотоке долго, то используется ключ `--globals-only`, таким образом выгружаются только глобальные объекты кластера. А БД можно выгрузить в многопотоке через `pg_dump`. Чтобы восстановиться из такой копии, нужно воспользоваться `psql -U postgres -f file.sql`.  
  
### Физические бекапы.  
  
Физически скопировать данные кластера или сделать снапшот ФС - холодный бекап.
[Подсказка.](https://gitlab.ru/sanjesss/alfa-tasks/-/blob/master/DBA3/PostgreSQL_DBA3_backup.md#%D1%84%D0%B8%D0%B7%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B5%D0%B7%D0%B5%D1%80%D0%B2%D0%BD%D0%B0%D1%8F-%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F)  
Горячий бекап делается через pg_basebackup. Сначала нужно чекнуть параметры, с 10 они по умолчанию нужные: `SELECT name, setting FROM pg_settings WHERE name IN ('wal_level', 'max_wal_senders', 'max_replication_slots');`. Также должна быть роль с правами login и replication. Если хотим развернуть сервер сразу после копии, то делаем `pg_basebackup -U user --pgdata=/usr/local/pgsql-10.5-2/data/`. Туда просто скопируются данные из нашего работающего кластера в реалтайме. Если добавим аргументы `--format=tar`, то получим копию в архиве. Чтобы запустить такой сервер, ему нужно заменить порт и запустить.  
  
### Архивирование журнала.  
  
Создаем директорию для непрерывной архивации, чтобы у пг был туда доступ. Добавляем два параметра и делаем рестарт сервера:
```
alter system set archive_mod=on;
alter system set archive_command='test ! -f /var/lib/postgresql/archive/%f && cp %p /var/lib/postgresql/archive/%f'
```
Для копирования журнала на другой сервер из директории архива:`scp -r archive postgres@sbb-jabberdbn:/usr/local/pgsql-10.5/`
Создаем БД для теста. Для просмотра сегмента ВАЛ, который используется сейчас пользуемся функцией `select pg_walfile_name(pg_current_wal_lsn());`. Чтобы переключиться на следующий сегмент используем функцию `select pg_switch_wal();`. Чекаем запись ВАЛ файлов в архив.  
  
Восстанавливаемся из pg_basebackup и архива журнала. Делаем pg_basebackup с аргументом `--wal-method=none`. В файле $PGDATA/backup_label будет отметка, с какого журнала ВАЛ начинать восстановление. Чтобы начать восстановление нам нужно поменять порт на кластере, создать директорию для архивов второго кластера и сменить запись о директории в postgresql.auto.conf. Далее нужно создать файл для восстановления в $PGDATA файл recovery.conf. В него нужно записать `restore_command='cp /var/lib/postgresql/archive/%f %p'`. Стартуем сервер. Он применит записи WAL и файл переименуется в recovery.done.  
  
### Физическая репликация.  
  
Создание реплики. На мастере нужно принудительно создать слот репликации `select pg_create_physical_replication_slot('slot');`. Информацию о слотах можно получить по `select * from pg_replication_slots;`. Информацию о состоянии репликации можно получить по функции `select * from pg_stat_replication;`. Чтобы завести реплику нужно выполнить `pg_basebackup -U user --pgdata/usr/local/pgsql-10.5-2 -R --slot=slot`. Меняем порт и добавляем `hot_standby=on` в postgresql.auto.conf, чтобы реплика была доступна для чтения. После этого можно стартовать. Функция `select pg_is_in_recovery();` отличает мастера от реплики.  
  
Для запуска реплики достаточно сделать `pg_ctl promote` на реплике.  
  
### pg_rewind  
  
Сначала делаем слот репликации на новом мастере. Останавливаем бывший мастер, с бывшего мастера вводим: `pg_rewind -D $PGDATA --source-server='host= user= port=' -P`.  
Нужно исправить postgresql.auto.conf:
```
postgres$ echo 'hot_standby = on' > /var/lib/postgresql/10/alpha/postgresql.auto.conf
```
И нужно сделать recovery.conf, чтобы он стал репликой:
```
postgres$ echo $'standby_mode = \'on\'' > /var/lib/postgresql/10/alpha/recovery.conf
postgres$ echo $'primary_conninfo = \'user=postgres port=5433\'' >> /var/lib/postgresql/10/alpha/recovery.conf
postgres$ echo $'primary_slot_name = \'replica\'' >> /var/lib/postgresql/10/alpha/recovery.conf
```
